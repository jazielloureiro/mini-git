#ifndef UTILITIES_H
#define UTILITIES_H

typedef struct {
	uint32_t begin, end;
	char *chars;
} Sliced_str;

void write_diff_file(Sliced_str str1, Sliced_str str2, FILE *diff);

char *patch(char *input, FILE *diff);

#endif
